#!/usr/bin/env python3
#! -*- coding:utf-8 -*-
vocales=['a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U']
def vocal_consonante(palabra):
    num_vocales = 0
    num_consonanetes = 0
    for i in range(0, len(palabra)):
        a = 0
        for j in range(0, 9):
            if palabra[j]==" ":
                continue
            elif vocales[j] == palabra[i]:
                print("La letra", palabra[i], "es vocal")
                a = 1
                num_vocales = num_vocales + 1
                break
        if a == 0:
            print("La letra", palabra[i], "es consonante")
            num_consonanetes =  num_consonanetes + 1
    print("Número de vocales:", num_vocales, "Número de consonantes", num_consonanetes)
palabra = input("ingrese una palabra")
vocal_consonante((palabra))
