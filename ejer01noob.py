#!/usr/bin/env python3
#! -*- coding:utf-8 -*-
#función que establece el promedio de los números
def promedio(lista):
    print("El promedio es:", sum(lista) / len(lista))
# función que establece el cuadrado de los números
def cuadrados(lista):
    print("Los datos de la lista al cuadrado son:")
    for i in range(0, len(lista)):
        print(lista[i]**2)
# función que establece la palabra más grande
def cuenta_letras(lista):
    lista.sort()
    print("La palabra más larga es:", lista[len(lista)-1])
#función que hace de main y que valida los valores y el tipo de datos
def tipo_datos():
    try:
        lista_promedio = [7.0, 4.0, 1.7]
        lista_cuadrado = [2, 3, 4, 5, 10]
        lista_palabras = ['Hola', 'Chao', 'paralelepipedo', 'mundo']
        funcion_a_usar = int(input("1. Promedio\n2. Cuadrado\n3. Palabra más larga\n"))
        #condición para conocer que función desea usar el usuario
        if (funcion_a_usar == 1):
            promedio(lista_promedio)
        if (funcion_a_usar == 2):
            cuadrados(lista_cuadrado)
        if (funcion_a_usar == 3):
            cuenta_letras(lista_palabras)
        #excepciones que validan los datos, de lo contrario el programa se reinicia
    except TypeError:
        print("El Tipo ingresado no es valido")
        tipo_datos()
    except ValueError:
        print("El Valor ingresado no es valido")
        tipo_datos()
#se llama a la función principal
tipo_datos()