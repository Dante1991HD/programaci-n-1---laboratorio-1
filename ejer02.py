#!/usr/bin/env python3
#! -*- coding:utf-8 -*-
def cuenta_vocales_consonantes():
    contador = 0
    vocales = "aAeEiIoOuU"
    try:
        palabra = str(input("ingrese una palabra\n"))
        largo = int(len(palabra))
        for i in range(largo):
            if palabra[i] == ' ':
                largo = largo - 1
            elif palabra[i] in vocales:
                contador = contador + 1
        print("La cantidad de vocales es de:", contador, "La cantidad de consonantes es de:", largo - contador,"\n")
    except TypeError:
        print("El Tipo ingresado no es valido, el programa se reiniciará")
        cuenta_vocales_consonantes()
    except ValueError:
        print("El Valor ingresado no es valido, el programa se reiniciará")
        cuenta_vocales_consonantes()
    menu()
def vocal_consonante():
    vocales = "aAeEiIoOuU"
    try:
        letra = str(input("ingrese una Letra (si ingresa más de una se considerará solo la primera)\n"))
        if letra[0] in vocales:
            print("La letra es vocal\n")
        else:
            print("La letra es consonanate\n")
    except TypeError:
        print("El Tipo ingresado no es valido, el programa se reiniciará")
        vocal_consonante()
    except ValueError:
        print("El Valor ingresado no es valido, el programa se reiniciará")
        vocal_consonante()
    menu()
def menu():
    try:
        seleccion = int(input("1.Identificador de vocal/consonante\n2.contador de vocales/consonantes\n3.cerrar el programa\n"))
        if seleccion == 1:
            vocal_consonante()
        elif seleccion == 2:
            cuenta_vocales_consonantes()
        elif seleccion == 3:
            exit()
        else:
            print("Error, el programa se reiniciará")
            menu()
    except TypeError:
        print("El Tipo ingresado no es valido, el programa se reiniciará")
        menu()
    except ValueError:
        print("El Valor ingresado no es valido, el programa se reiniciará")
        menu()
menu()
